package controller;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import org.openstreetmap.gui.jmapviewer.DefaultMapController;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.ICoordinate;
import model.Coordinate;
import model.Edge;
import model.Graph;
import model.Vertex;

public class Controller {

	private Graph graph;
	private JMapViewer map;
	private DefaultMapController mapController;
	private ArrayList<MapPolygonImpl> paths;
	private ArrayList<Graph> clusters;

	public Controller() {
		map = new JMapViewer();
		map.setBounds(10, 0, 988, 660);
		map.setZoomControlsVisible(false);
		map.setDisplayPosition(new MapMarkerDot(-34.530, -58.7020), 7);
		map.setLayout(null);
		paths = new ArrayList<MapPolygonImpl>();
		mapController = new DefaultMapController(map);
		mapController.setMovementMouseButton(MouseEvent.BUTTON1);
	}

	public void detectCoordinate(String instance) {
		try {
			cleanMap();
			graph = new Graph();
			FileInputStream fis = new FileInputStream(instance);
			Scanner scanner = new Scanner(fis);
			while (scanner.hasNext()) {
				Coordinate cor = new Coordinate(Double.parseDouble(scanner.next()), Double.parseDouble(scanner.next()));
				Vertex vertex = new Vertex(cor);
				graph.addVertex(vertex);
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void drawVertexes() {
		for (int i = 0; i < graph.getVertexesAmount(); i++) {
			map.addMapMarker(new MapMarkerDot(graph.getVertex(i).getLatitude(), graph.getVertex(i).getLongitude()));
		}
	}

	private void drawRecolouredVertexes() {
		Random rand = new Random();
		for (Graph graph : clusters) {
			Color randomColor = new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
			for (int i = 0; i < graph.getVertexesAmount(); i++) {
				MapMarkerDot marker = new MapMarkerDot(graph.getVertex(i).getLatitude(),
						graph.getVertex(i).getLongitude());
				marker.setBackColor(randomColor);
				map.addMapMarker(marker);
			}
		}
	}

	public void drawMST() {
		cleanMap();
		for (int i = 0; i < graph.getMSTEdgesAmount(); i++) {
			Edge edge = graph.getMSTEdge(i);
			ICoordinate coordinatev1 = new MapMarkerDot(edge.getOrigin().getLatitude(),
					edge.getOrigin().getLongitude());
			ICoordinate coordinatev2 = new MapMarkerDot(edge.getDestination().getLatitude(),
					edge.getDestination().getLongitude());
			MapPolygonImpl way = new MapPolygonImpl(coordinatev1, coordinatev2, coordinatev1);
			map.addMapPolygon(way);
			paths.add(way);
		}
		drawVertexes();
	}

	public void drawClusters(BigDecimal standard) {
		if (standard != null) {
			clusters = graph.cluster(standard.divide(new BigDecimal(100)));
			cleanMap();
			for (Graph g : clusters) {
				for (int i = 0; i < g.getMSTEdgesAmount(); i++) {
					Edge edge = g.getMSTEdge(i);
					ICoordinate coordinatev1 = new MapMarkerDot(edge.getOrigin().getLatitude(),
							edge.getOrigin().getLongitude());
					ICoordinate coordinatev2 = new MapMarkerDot(edge.getDestination().getLatitude(),
							edge.getDestination().getLongitude());
					MapPolygonImpl way = new MapPolygonImpl(coordinatev1, coordinatev2, coordinatev1);
					map.addMapPolygon(way);
					paths.add(way);
				}
			}
			drawRecolouredVertexes();
		}
	}

	public String clustersWeightToString() {
		if (clusters != null) {
			String text = "";
			for (int i = 0; i < clusters.size(); i++) {
				Graph actualGraph = clusters.get(i);
				text += "Peso del cluster " + (i + 1) + ": "
						+ actualGraph.getWeight().multiply(new BigDecimal(100)).toString() + "\n";
			}
			return text;
		}
		return null;
	}

	// GETTERS
	public JMapViewer getMap() {
		return map;
	}

	public ArrayList<Graph> getClusters() {
		return clusters;
	}

	// AUXILIAR FUNCTIONS
	private void cleanMap() {
		map.removeAllMapPolygons();
		map.removeAllMapMarkers();
		paths.clear();
	}
}