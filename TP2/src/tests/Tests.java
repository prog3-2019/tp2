package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Coordinate;
import model.Edge;
import model.Graph;
import model.Vertex;

class Tests {
	private Graph g;
	private Graph g2;

	@BeforeEach
	void init() {
		g = new Graph();
		g2 = new Graph();
	}

	@Test
	void vertexEquasTestFalse() {
		boolean acum = false;
		Vertex v = new Vertex(new Coordinate(3.0, 4.6));
		Vertex v2 = new Vertex(new Coordinate(3.5, 4.6));
		acum = acum && v.equals(v2) && v2.equals(null);
		assertFalse(acum);

	}

	@Test
	void vertexEquasTestTrue() {
		Vertex v = new Vertex(new Coordinate(3.0, 4.6));
		Vertex v2 = new Vertex(new Coordinate(3.0, 4.6));
		assertTrue(v.equals(v2));

	}

	@Test
	void vertexConstructorTest() {
		assertThrows(RuntimeException.class, () -> new Vertex(null));
	}

	@Test
	void edgeWeightTest() {
		Edge e = new Edge(new Vertex(new Coordinate(6, 2)), new Vertex(new Coordinate(10, 12)));
		assertEquals(e.getWeight(), new BigDecimal("10.770329614269007"));
	}

	@Test
	void edgeContainsTrueTest() {
		Vertex v = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3.0, 4.6));
		Edge e = new Edge(v, v2);
		assertTrue(e.contains(v2));
	}

	@Test
	void edgeContainsFalseTest() {
		Vertex v = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3.0, 4.6));
		Vertex v3 = new Vertex(new Coordinate(3.1, 4.5));
		Edge e = new Edge(v, v2);
		assertFalse(e.contains(v3));
	}

	@Test
	void edgeConstructorTest() {
		Vertex v = new Vertex(new Coordinate(12, 8));
		assertThrows(RuntimeException.class, () -> new Edge(v, null));
	}

	@Test
	void edgeComparetoTest() {
		int a = 0;
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3, 4));
		Vertex v3 = new Vertex(new Coordinate(8, 6));
		Vertex v4 = new Vertex(new Coordinate(1, 0));
		Edge e1 = new Edge(v1, v2);
		Edge e2 = new Edge(v3, v4);
		Edge e3 = new Edge(v1, v2);
		a += e1.compareTo(e2) + e2.compareTo(e1) + e3.compareTo(e1);
		assertEquals(a, 0);

	}

	@Test
	void graphTest() {
		int acum = 0;
		acum += g.getEdgesAmount() + g.getMSTEdgesAmount() + g.getVertexesAmount();
		assertEquals(acum, 0);
	}

	@Test
	void graphAddVertexExceptionTest() {
		assertThrows(RuntimeException.class, () -> g.addVertex(null));
	}

	@Test
	void graphAmountVertexesTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3, 4));
		Vertex v3 = new Vertex(new Coordinate(8, 6));
		g.addVertex(v1);
		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		assertEquals(g.getVertexesAmount(), 3);

	}

	@Test
	void graphAmountEdgesTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3, 4));
		Vertex v3 = new Vertex(new Coordinate(8, 6));
		Vertex v4 = new Vertex(new Coordinate(5, 7));
		Vertex v5 = new Vertex(new Coordinate(13, 14));
		Vertex v6 = new Vertex(new Coordinate(58, 60));
		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v4);
		g.addVertex(v5);
		g.addVertex(v6);
		g.addVertex(v3);
		g.addVertex(v4);
		assertEquals(g.getEdgesAmount(), 30);

	}

	@Test
	void getVertexBorderTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		g.addVertex(v1);
		assertThrows(RuntimeException.class, () -> g.getVertex(-1));
	}

	@Test
	void getVertexBorderExtremeTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		g.addVertex(v1);
		assertThrows(RuntimeException.class, () -> g.getVertex(2));
	}

	@Test
	void getWeightGraphTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3, 4));
		g.addVertex(v1);
		g.addVertex(v2);
		g2.addVertex(v1);
		BigDecimal num = g.getWeight().add(g2.getWeight());
		assertEquals(num, new BigDecimal("19.697715603592208"));
	}

	@Test
	void getWeightMstExceptionTest() {
		assertThrows(RuntimeException.class, () -> g.getMSTWeight());
	}

	@Test
	void getWeightMstTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3, 4));
		Vertex v3 = new Vertex(new Coordinate(8, 6));
		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		assertEquals(g.getMSTWeight(), new BigDecimal("9.857300762134084"));
	}

	@Test
	void clusterTest() {
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(10, 4));
		Vertex v3 = new Vertex(new Coordinate(16, 6));
		Vertex v4 = new Vertex(new Coordinate(11, 18));
		Vertex v5 = new Vertex(new Coordinate(-5, 31));
		Vertex v6 = new Vertex(new Coordinate(-11, 20));
		Vertex v7 = new Vertex(new Coordinate(39, 17));
		g.addVertex(v1);
		g.addVertex(v2);
		g.addVertex(v3);
		g.addVertex(v4);
		g.addVertex(v5);
		g.addVertex(v6);
		g.addVertex(v7);
		assertEquals(g.cluster(new BigDecimal("15")).size(), 3);
	}

	@Test
	void toStringGraphTest() {
		String ret = "";
		Vertex v1 = new Vertex(new Coordinate(12, 8));
		Vertex v2 = new Vertex(new Coordinate(3, 4));
		g.addVertex(v1);
		ret += g.toString();
		g.addVertex(v2);
		ret += g.toString();
		assertEquals(ret,
				"\n \tVertice [12.0, 8.0]\n{ Vertice origen [12.0, 8.0] - Vertice destino [3.0, 4.0] -> Distancia: 9.848857801796104}\n{ Vertice origen [3.0, 4.0] - Vertice destino [12.0, 8.0] -> Distancia: 9.848857801796104}\n");
	}

	@Test
	void coordinateEqualsTest() {
		boolean acum = false;
		Coordinate cor1 = new Coordinate(10, 1);
		Coordinate cor2 = new Coordinate(10, 1);
		Coordinate cor3 = new Coordinate(1, 10);
		acum = acum || cor1.equals(cor2) || cor1.equals(cor3) || cor3.equals(null);
		assertEquals(acum, true);
	}

}
