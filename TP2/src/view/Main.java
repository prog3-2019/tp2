package view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import controller.Controller;

public class Main {

	private Controller controller;
	private JFrame frame;
	private JPanel mapPanel;
	private JTextField standardTextField;
	private JTextArea clustersWeightTextArea;
	private ButtonGroup radioButtonsGroup;
	private ArrayList<JLabel> labels;
	private ArrayList<JButton> buttons;
	private ArrayList<JInternalFrame> internalFrames;
	private ArrayList<JRadioButton> radioButtons;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Main() {
		initialize();
	}

	private void initialize() {
		controller = new Controller();

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 1024, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		internalFrames = new ArrayList<JInternalFrame>();
		internalFrames.add(new JInternalFrame("Pesos de los clusters"));
		internalFrames.add(new JInternalFrame("ERROR"));
		internalFrames.get(0).setBounds(574, 150, 346, 501);
		internalFrames.get(1).setBounds(368, 267, 231, 152);
		for (JInternalFrame jif : internalFrames) {
			jif.setClosable(true);
			jif.getContentPane().setLayout(null);
			jif.setVisible(false);
			controller.getMap().add(jif);
		}

		clustersWeightTextArea = new JTextArea();
		clustersWeightTextArea.setEditable(false);
		clustersWeightTextArea.setBounds(0, 0, 480, 471);
		internalFrames.get(0).getContentPane().add(clustersWeightTextArea);

		mapPanel = new JPanel();
		mapPanel.setBounds(0, 0, 1018, 739);
		mapPanel.setLayout(null);
		mapPanel.add(controller.getMap());
		frame.getContentPane().add(mapPanel);

		labels = new ArrayList<JLabel>();
		labels.add(new JLabel("No hay clusters"));
		labels.add(new JLabel("Criterio de clusterizado:          km"));
		labels.add(new JLabel("Instancia:"));
		labels.get(0).setBounds(720, 676, 220, 14);
		labels.get(1).setBounds(720, 701, 190, 14);
		labels.get(2).setBounds(20, 690, 57, 14);
		for (JLabel jl : labels)
			mapPanel.add(jl);

		labels.add(new JLabel("<html> �El criterio de clusterizado <br/> es inv�lido! </html>"));
		labels.get(3).setBounds(10, 23, 205, 58);
		labels.get(3).setHorizontalAlignment(SwingConstants.CENTER);
		labels.get(3).setFont(new Font("Tahoma", Font.PLAIN, 14));
		internalFrames.get(1).getContentPane().add(labels.get(3));

		buttons = new ArrayList<JButton>();
		buttons.add(new JButton("Dibujar AGM"));
		buttons.add(new JButton("Dibujar clusters"));
		buttons.add(new JButton("Aceptar"));
		buttons.add(new JButton("Actualizar"));
		buttons.get(0).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.drawMST();
			}
		});
		buttons.get(1).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.drawClusters(detectStandard());
				updateClustersLabel();
				clustersWeightTextArea.setText(controller.clustersWeightToString());
			}
		});
		buttons.get(2).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				internalFrames.get(1).setVisible(false);
			}
		});
		buttons.get(0).setBounds(402, 687, 130, 23);
		buttons.get(1).setBounds(560, 687, 130, 23);
		buttons.get(2).setBounds(59, 92, 109, 23);
		buttons.get(3).setBounds(275, 687, 102, 23);
		for (JButton jb : buttons)
			mapPanel.add(jb);
		internalFrames.get(1).getContentPane().add(buttons.get(2));

		standardTextField = new JTextField();
		standardTextField.setBounds(862, 701, 22, 17);
		mapPanel.add(standardTextField);
		standardTextField.setColumns(10);

		buttons.get(3).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.detectCoordinate(detectInstance());
				controller.drawVertexes();
				labels.get(0).setText("No hay clusters");
			}
		});

		radioButtons = new ArrayList<JRadioButton>();
		radioButtonsGroup = new ButtonGroup();
		for (int i = 0; i < 5; i++) {
			radioButtons.add(new JRadioButton("" + (i + 1)));
		}
		int x = 82;
		for (JRadioButton rb : radioButtons) {
			radioButtonsGroup.add(rb);
			rb.setBounds(x, 687, 37, 23);
			x += 39;
			mapPanel.add(rb);
		}
		radioButtons.get(0).setSelected(true);

		controller.detectCoordinate(detectInstance());
		controller.drawVertexes();
	}

	// AUXILIAR FUNCTIONS

	private BigDecimal detectStandard() {
		try {
			if (!standardTextField.getText().isEmpty() && Double.parseDouble(standardTextField.getText()) > 0) {
				return new BigDecimal(Double.valueOf(standardTextField.getText()));
			}
		} catch (NumberFormatException ex) {
			internalFrames.get(1).setVisible(true);
		}
		return null;
	}

	private void updateClustersLabel() {
		if (detectStandard() != null) {
			labels.get(0).setText("Cantidad de clusters: " + controller.getClusters().size());
			internalFrames.get(0).setVisible(true);
		} else {
			internalFrames.get(1).setVisible(true);
		}
	}

	private String detectInstance() {
		String instance = "";
		for (int i = 0; i < radioButtons.size(); i++) {
			if (radioButtonsGroup.getSelection().equals(radioButtons.get(i).getModel())) {
				instance = "instancia" + (i + 1) + ".txt";
			}
		}
		return instance;
	}
}
