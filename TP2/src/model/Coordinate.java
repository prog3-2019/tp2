package model;
public class Coordinate {

	private double latitude;
	private double longitude;

	public Coordinate(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coordinate other = (Coordinate) obj;
		if (this.latitude != other.latitude)
			return false;
		if (this.longitude != other.longitude)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return latitude + ", " + longitude;
	}

	// GETTERS

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}
}
