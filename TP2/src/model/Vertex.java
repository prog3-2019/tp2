package model;
import java.math.BigDecimal;

public class Vertex {

	private Coordinate coordinate;

	public Vertex(Coordinate coordinate) {
		if (coordinate == null)
			throw new RuntimeException("El parametro no puede ser null");
		this.coordinate = coordinate;
	}

	public double getLongitude() {
		return coordinate.getLongitude();
	}

	public double getLatitude() {
		return coordinate.getLatitude();
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public BigDecimal calculateDistance(Vertex vertex) {
		double x = Math.max(getLongitude(), vertex.getLongitude()) - Math.min(getLongitude(), vertex.getLongitude());
		double y = Math.max(getLatitude(), vertex.getLatitude()) - Math.min(getLatitude(), vertex.getLatitude());
		return BigDecimal.valueOf(Math.sqrt(x * x + y * y));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (!(o instanceof Vertex))
			return false;
		Vertex other = (Vertex) o;
		if (getLongitude() == other.getLongitude() && getLatitude() == other.getLatitude())
			return true;
		return false;
	}

	@Override
	public String toString() {
		return "\n \tVertice [" + coordinate + "]";
	}
}