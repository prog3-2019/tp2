package model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;

public class Graph {

	private ArrayList<Vertex> vertexes;
	private ArrayList<Edge> edges;
	private ArrayList<Edge> mstEdges;

	public Graph() {
		vertexes = new ArrayList<Vertex>();
		edges = new ArrayList<Edge>();
		mstEdges = new ArrayList<Edge>();
	}

	public void addVertex(Vertex vertex) {
		if (vertex == null)
			throw new RuntimeException("El parametro no puede ser null");
		if (!vertexes.contains(vertex)) {
			updateEdges(vertex);
			updateMSTEdges();
			vertexes.add(vertex);
		}
	}

	public ArrayList<Graph> cluster(BigDecimal condition) {
		ArrayList<Graph> clusters = new ArrayList<Graph>();
		for (Edge edge : mstEdges) {
			if (edge.getWeight().compareTo(condition) == -1) {
				clusters = createSubgraphs(clusters, edge);
			}
		}
		return findVertexAlone(clusters);
	}

	@Override
	public String toString() {
		String ret = "";
		if (this.getVertexesAmount() == 1) {
			ret += this.getVertex(0).toString() + "\n";
		}
		for (int i = 0; i < edges.size(); i++) {
			ret += edges.get(i).toString() + "\n";
		}
		return ret;
	}

	// GETTERS
	public int getVertexesAmount() {
		return vertexes.size();
	}

	public Vertex getVertex(int i) {
		if (getVertexesAmount() <= i || i < 0)
			throw new RuntimeException("Indice inv�lido");
		return vertexes.get(i);
	}

	public int getEdgesAmount() {
		return edges.size();
	}


	public int getMSTEdgesAmount() {
		return mstEdges.size();
	}

	public Edge getMSTEdge(int i) {
		if (getMSTEdgesAmount() <= i || i < 0)
			throw new RuntimeException("Indice invalido");
		return mstEdges.get(i);
	}

	public BigDecimal getWeight() {
		BigDecimal weight = new BigDecimal(0);
		if (getVertexesAmount() > 1) {
			for (Edge e : edges) {
				BigDecimal aux = weight.add(e.getWeight());
				weight = aux;
			}
		}
		return weight;
	}

	public BigDecimal getMSTWeight() {
		if (getMSTEdgesAmount() >= 1) {
			BigDecimal ret = new BigDecimal(0);
			for (Edge e : mstEdges) {
				BigDecimal aux = ret.add(e.getWeight());
				ret = aux;
			}
			return ret;
		} else {
			throw new RuntimeException("El grafo no posee AGM");
		}
	}

	// AUXILIAR FUNCTIONS
	private void updateEdges(Vertex vertex) {
		for (int i = 0; i < getVertexesAmount(); i++) {
			addEdge(new Edge(getVertex(i), vertex));
			addEdge(new Edge(vertex, getVertex(i)));
		}
	}

	private void addEdge(Edge edge) {
		if (!edges.contains(edge))
			edges.add(edge);
	}

	private void updateMSTEdges() {
		mstEdges = new ArrayList<Edge>();
		generateMST();
	}

	private void generateMST() {
		ArrayList<Edge> edges = new ArrayList<Edge>(this.edges);
		edges.sort(new Comparator<Edge>() {
			@Override
			public int compare(Edge edge1, Edge edge2) {
				return edge1.compareTo(edge2);
			}
		});
		ArrayList<Vertex> visitedVertexes = new ArrayList<Vertex>();
		if (edges.size() >= 1) {
			Vertex vertex = edges.get(0).getOrigin();
			visitedVertexes.add(vertex);
		}
		while (edges.size() >= 1) {
			Edge edge = minorEdge(visitedVertexes, edges);
			if (edge != null) {
				mstEdges.add(edge);
				if (visitedVertexes.contains(edge.getOrigin()))
					visitedVertexes.add(edge.getDestination());
				else
					visitedVertexes.add(edge.getOrigin());
			} else
				edges.clear();
		}
	}

	private Edge minorEdge(ArrayList<Vertex> vertexes, ArrayList<Edge> edges) {
		for (int i = 0; i < edges.size(); i++) {
			Edge edge = edges.get(i);
			if (vertexes.contains(edge.getOrigin()) && !vertexes.contains(edge.getDestination())
					|| !vertexes.contains(edge.getOrigin()) && vertexes.contains(edge.getDestination())) {
				return edge;
			}
		}
		return null;
	}

	private static Graph createNewGraph(Edge edge) {
		Graph g = new Graph();
		g.addEdge(edge);
		g.addVertex(edge.getOrigin());
		g.addVertex(edge.getDestination());
		return g;
	}

	private ArrayList<Graph> createSubgraphs(ArrayList<Graph> subgraphs, Edge e) {
		boolean acum = false;
		if (subgraphs.size() == 0) {
			subgraphs.add(createNewGraph(e));
			return subgraphs;
		}
		for (Graph g : subgraphs) {
			if ((existsVertex(g, e.getOrigin()) && !existsVertex(g, e.getDestination()))
					|| (!existsVertex(g, e.getOrigin()) && existsVertex(g, e.getDestination()))) {
				g.addVertex(e.getOrigin());
				g.addVertex(e.getDestination());
				g.addEdge(e);
				acum = true;
			}
		}
		if (!acum) {
			Graph graph = new Graph();
			graph.addEdge(e);
			graph.addVertex(e.getOrigin());
			graph.addVertex(e.getDestination());
			subgraphs.add(graph);
		}
		return subgraphs;
	}

	private boolean existsVertex(Graph g, Vertex v) {
		boolean acum = false;
		for (Edge e : g.edges) {
			acum = acum || e.getOrigin().equals(v) || e.getDestination().equals(v);
		}
		return acum;

	}

	private ArrayList<Graph> findVertexAlone(ArrayList<Graph> clusters) {
		for (Vertex v : this.vertexes) {
			boolean acum = false;
			for (Graph g : clusters) {
				acum = acum || existsVertex(g, v);
			}
			if (!acum) {
				Graph graph = new Graph();
				graph.addVertex(v);
				clusters.add(graph);
			}
		}
		return clusters;
	}

}