package model;
import java.math.BigDecimal;

public class Edge implements Comparable<Edge> {

	private Vertex origin;
	private Vertex destination;
	private BigDecimal weight;

	public Edge(Vertex origin, Vertex destination) {
		if (origin == null || destination == null || origin.equals(destination))
			throw new RuntimeException("Vertices invalidos");
		this.origin = origin;
		this.destination = destination;
		weight = origin.calculateDistance(destination);
	}

	public Vertex getOrigin() {
		return origin;
	}

	public Vertex getDestination() {
		return destination;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public boolean contains(Vertex vertex) {
		if (origin.equals(vertex) || destination.equals(vertex))
			return true;
		return false;
	}

	@Override
	public int compareTo(Edge other) {
		return weight.compareTo(other.getWeight()) == 1 ? 1 : weight.compareTo(other.getWeight()) == 0 ? 0 : -1;
	}

	@Override
	public String toString() {
		return "{ Vertice origen [" + origin.getCoordinate() + "] - Vertice destino [" + destination.getCoordinate()
				+ "] -> Distancia: " + weight + "}";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (!(o instanceof Edge))
			return false;
		Edge other = (Edge) o;
		if (other.getOrigin().equals(origin) && other.getDestination().equals(destination))
			return true;
		return false;
	}
}